const mongoose = require('mongoose');

//crearemos el model del usuario
const UsuariosSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        //trim elimina espacios en blanco
        trim: true
    },
    apellido: {
        type: String,
        required: true,
        trim: true
    },
    cargo: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    //en registro se registrara la fecha de registro
    registro: {
        type: Date,
        default: Date.now()
    }
});


module.exports = mongoose.model('Usuario', UsuariosSchema);