const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator'); 

exports.autentificarUsuario = async (req, res) => {
    
    //Revisar si hay errores, estos msjs estan creados en usuarios.js check
    const errores = validationResult(req);
    if(!errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() });
    }

    //Extraer el email y password
    const { email, password } = req.body;

    try {
        //Revisar que sea un usuario registrado
        let usuario = await Usuario.findOne({ email });
        if(!usuario) {
            return res.status(400).json({msg: "El usuario no existe"});
        }

        //Revisar el password
        const passCorrecto = await bcryptjs.compare(password, usuario.password);
        if(!passCorrecto) {
            return res.status(400).json({msg: "El password es incorrecto"});
        }

        //si todo es correcto crear y Firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 3600 //una hora en segundos
            //este es un callback-> }, (error, token) => {
        }, (error, token) => {
            if(error) throw error;

            //Mensaje de confirmacion
            res.json({ token });
            //console.log(token)
        });

    } catch (error) {
        console.log(error);
    }
};

//Obtiene qué usuario esta autenticado
exports.usuarioAutenticado = async (req, res) => {
    try {
        //Acá vamos a traer todo el resgitro del usuario que el id que estamos enviando
        //.select le dice a mongo que no quiero que se muestre este campos
        const usuario = await Usuario.findById(req.usuario.id).select("-password");
        res.json({usuario});

    } catch (error) {
        console.log(error);
        res.status(500).json({msg: "Hubo un error"});
    }
}  