const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator'); 
const { response } = require('express');

exports.crearUsuario = async (req, res) => {
    console.log(req.body);

    //Revisar si hay errores, estos msjs estan creados en usuarios.js check
    const errores = validationResult(req);
    if(!errores.isEmpty() ) {
        return res.status(400).json({errores: errores.array() })
    }

    //Extraer email y password
    const { email, password } = req.body;

    try {
        //Revisar que el usuario registrado sea unico con findOne
        let usuario = await Usuario.findOne({ email });

        if(usuario) {
            return res.status(400).json({ msg: "El usuario ya existe" });
        }

        //crea el nuevo usuario
        usuario = new Usuario(req.body);

        //Hashear el password, salt generara password unico
        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash(password, salt );

        //guardar usuario
        await usuario.save();

        //Crear y firmar el JWT
        /*se almacenaran datos del usuario que se estan guardando en usuario.save
        guardamos como payload el id del usuario que se estamos firmando
        tendremos un token con el id de user, cuando inicie sesion con ese id
        podremos consultar la base de datos con sus proyectos creados*/
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        //Firmar el JWT
        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 3600 //una hora en segundos
            
        }, (error, token) => {
            if(error) throw error;

            //Mensaje de confirmacion
            res.json({ token });
            //console.log(token)
        });

        //mensaje de confirmacion
        //res.send({ msg: "Usuario creado correctamente"})
    } catch (error) {
        //console.log(error);
        res.status(400).send("Hubo un error");
    }
};

// Obtiene los usuarios registrados
exports.obtenerUsuarios = async(req, res = response) => {

    const users = await Usuario.find()
                               .populate('usuario', 'nombre');

    res.json({
        ok: true,
        users
    });
}

exports.actualizarUsuarios = async(req, res, response) => {

    try{    

        const { usuario, nombre, apellido, cargo, email} = req.body;

        let user = await Usuario.findById(req.params.id);

        if(!user) {
            return res.status(404).json({msg: "No existe el usuario "});
        }

        //Extraer usuario
        const existeUsuario = await Usuario.findById(usuario);
        /*
        
        */
        const nuevoUsario = {};
            nuevoUsario.nombre = nombre;
            nuevoUsario.apellido = apellido;
            nuevoUsario.cargo = cargo;
            nuevoUsario.email = email;

        //guardar usuario
        user = await Usuario.findByIdAndUpdate({ _id : req.params.id }, nuevoUsario, {new: true} );
        res.json({ user })

        }catch (error){
            console.log(error);
            res.status(500).send("Hubo un error");
    }
    /*
    res.json({
        ok: true,
        user
    })
    */
}

//