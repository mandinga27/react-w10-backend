//Rutas para crear usuarios
const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarioController');
const { check } = require('express-validator');


//Crea un usuario
//Api usuarios
router.post('/',
    [
        check("nombre", "el nombre es obligatorio").not().isEmpty(),
        check("apellido", "el apellido es obligatorio").not().isEmpty(),
        check("cargo", "el cargo es obligatorio").not().isEmpty(),
        check("email", "Ingresa un email válido").isEmail(),
        check("password", "El password debe ser mínimo de 6 caracteres").isLength({ min: 6 })
    ],
    usuarioController.crearUsuario
);

// Obtener usuarios
router.get('/',

    usuarioController.obtenerUsuarios
);

module.exports = router;