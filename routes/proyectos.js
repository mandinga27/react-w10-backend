const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const proyectoController = require('../controllers/proyectoController');
const { check } = require('express-validator');

//Crea proyectos
// api/proyectos
router.post('/',
    auth,
    [
        check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()
    ],
    proyectoController.crearProyecto  
);

//Obtener todos los proyectos
// api/proyectos
router.get('/',
    auth,
    proyectoController.obtenerProyectos  
);

//Actualizar proyecto via ID
router.put('/:id',
    auth,
    [
        check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()
    ],
    proyectoController.actualizarProyecto
);

//Eliminar un Proyecto
router.delete('/:id',
    auth,
    proyectoController.eliminarProyecto
);

module.exports = router;